from django.shortcuts import render
import datetime

# Create your views here.
from django.http import HttpResponse


def index(request):
    return render(request,"story4.html")

def minigame(request):
    return render(request,"story4-minigame.html")

def time(request, timezone="0"):
    timezone = int(timezone)
    if(-12<timezone<14):
        time = datetime.datetime.now() + datetime.timedelta(hours=timezone)
        valid = True
    else:
        time = ""
        valid = False
    print(datetime.datetime.now())
    return render(request, "time.html", {'time':time, 'valid':valid})