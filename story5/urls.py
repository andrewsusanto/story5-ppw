from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index jadwal'),
    path('<int:indexmatkul>/',views.detailMatkul, name='detail matkul jadwal')
]