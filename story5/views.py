from django.shortcuts import render, redirect
import datetime
from .forms import JadwalForm
from .models import Jadwal, Tugas

# Create your views here.
def index(request):
    form = JadwalForm()
    if request.method == 'POST':
        forminput = JadwalForm(request.POST)
        if forminput.is_valid():
            data = forminput.cleaned_data
            data_input = Jadwal()
            data_input.nama_matkul = data['nama_matkul']
            data_input.nama_dosen = data['nama_dosen']
            data_input.jumlah_sks = data['jumlah_sks']
            data_input.deskripsi = data['deskripsi']
            data_input.semester_tahun = data['semester_tahun']
            data_input.ruang_kelas = data['ruang_kelas']
            data_input.save()
            current_data = Jadwal.objects.all()
            return render(request, 'jadwal.html', {'form':form, 'status': 'success', 'data' : current_data})
        else:
            current_data = Jadwal.objects.all()
            return render(request, 'jadwal.html', {'form':form, 'status': 'failed', 'data' : current_data})
    else:
        current_data = Jadwal.objects.all()
        return render(request, 'jadwal.html', {'form':form, 'data':current_data})


def detailMatkul(request, indexmatkul = 0):
    if indexmatkul != 0 and Jadwal.objects.filter(pk=indexmatkul).exists():
        current_data = Jadwal.objects.get(pk = indexmatkul)
        data_tugas = Tugas.objects.filter(nama_matkul = current_data).order_by("deadline")
        data_tugas_new = []
        for x in data_tugas :
            time_delta = x.deadline-datetime.datetime.now(datetime.timezone.utc)
            print(time_delta.days)
            if(time_delta.days*86400+time_delta.seconds<86400):
                color = "#ffcdd2"
            elif(time_delta.days*86400+time_delta.seconds<259200):
                color = "#ffcc80"
            else:
                color = "#bbdefb"
            
            data_tugas_new.append((x,color))
        if request.method == 'POST' and request.POST.get('action')=='delete':
            current_data.delete()
            return redirect(index)
        
        return render(request, 'detailjadwal.html', {'data':current_data, 'tugas':data_tugas_new})
    else:
        form = JadwalForm()
        current_data = Jadwal.objects.all()
        return render(request, 'jadwal.html', {'form':form, 'data':current_data})